#ifndef CONFIG_H
#define CONFIG_H

#define ONE_TEST 0

constexpr unsigned THREAD_MAX = 11;
constexpr unsigned long long STACK_MAX = 1024 * 1024 * 1024 * 30ull;
constexpr unsigned MAX_N = 9;
constexpr unsigned MAX_SIZE = MAX_N * MAX_N;

#endif // CONFIG_H
