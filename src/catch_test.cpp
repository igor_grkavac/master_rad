#include "Calculation.h"
#include "gtest/gtest.h"

// Resenja za    1   2   3   4   5   6   7   8   9   10
//               2   3   5   7  11  19  43  91 227  587
// Tacna resenja
// 2: 0 1

// 3: 0 1 2

// 4: 0 1 2 3

// 5: 0 1 2 3 4 5

// 6: 0 1 2 3 4 5 6 7 8 9

// 7: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 20 24 32

// 8: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27
// 28 29 30 31 32 33 34 35 36 37 38 39 40 42 44 45 48 56

static Calculation c;

TEST(RacunanjeResenja2, Determinanta) { EXPECT_EQ(3, c.calculation(2)); }

TEST(RacunanjeResenja3, Determinanta) { EXPECT_EQ(5, c.calculation(3)); }

TEST(RacunanjeResenja4, Determinanta) { EXPECT_EQ(7, c.calculation(4)); }

TEST(RacunanjeResenja5, Determinanta) { EXPECT_EQ(11, c.calculation(5)); }

TEST(RacunanjeResenja6, Determinanta) { EXPECT_EQ(19, c.calculation(6)); }

TEST(RacunanjeResenja7, Determinanta) { EXPECT_EQ(43, c.calculation(7)); }

// TEST(RacunanjeResenja8, Determinanta) { EXPECT_EQ(91, c.calculation(8)); }

// TEST(RacunanjeResenja9, Determinanta) { EXPECT_EQ(227, c.calculation(9)); }
