//
// Created by grkavac on 15.1.18..
//

#ifndef KANONSKI_OBLIK_CALCULATION_H
#define KANONSKI_OBLIK_CALCULATION_H
#include "config.h"
#include <mutex>
#include <vector>

#include <set>

class Calculation {
public:
  int calculation(unsigned test_case);
  void thread_calculate();
  void thread_det();
  void print_thread();
  static int det(char a[MAX_SIZE], unsigned n);

  class Matrix;

  static size_t matrixSize;
  std::set<uint8_t> solutions;
  std::set<std::vector<uint8_t>> solutions_rep;
  std::mutex solutions_mutex;
  std::mutex det_mutex;
  bool program_end = false;
};
#endif // KANONSKI_OBLIK_CALCULATION_H
