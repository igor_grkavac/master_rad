#include <algorithm>
#include <bitset>
#include <cmath>
#include <cstring>
#include <curses.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <term.h>
#include <thread>
#include <unistd.h>
#include <vector>

#include "Calculation.h"
#include "config.h"
#include "matrix.h"

using namespace std;

size_t Calculation::matrixSize;

class ThreadMatrix {
public:
  char mat[MAX_SIZE];
  unsigned size;
  ThreadMatrix(char *a, unsigned n) {
    size = n;

    memcpy(mat, a, size * size);
  }

  ~ThreadMatrix() {}
};

vector<unique_ptr<ThreadMatrix>> threadMatrices;

class Matrica;

static std::mutex unsolved_matrix_mutex;

// Basic det calculation
int Calculation::det(char mat[MAX_SIZE], unsigned n) {
  int d = 0;

  if (n == 3)
    return (mat[0] * ((mat[4] * mat[8]) - (mat[7] * mat[5])) -
            mat[1] * ((mat[3] * mat[8]) - (mat[6] * mat[5])) +
            mat[2] * ((mat[3] * mat[7]) - (mat[6] * mat[4])));
  else {

    char submat[(MAX_N - 1) * (MAX_N - 1)];

    for (unsigned c = 0; c < n; c++) {
      int subi = 0; // submatrix's i value
      for (unsigned i = 1; i < n; i++) {
        int subj = 0;

        for (unsigned j = 0; j < n; j++) {
          if (j == c)
            continue;
          submat[subi * (n - 1) + subj] = mat[i * n + j];
          subj++;
        }

        subi++;
      }
      d +=  mat[c] * det(submat, n - 1) * ((c%2)*2-1);
    }
  }

  return d;
}

static int pred = 0;

void Calculation::thread_det() {

  while (true) {
    solutions_mutex.lock();
    if (solutions_rep.size() == 0) {
      solutions_mutex.unlock();
      return;
    }

    pi_rep::Matrix m(*solutions_rep.begin());
    solutions_rep.erase(solutions_rep.begin());

    solutions_mutex.unlock();

    auto res = m.det();

    det_mutex.lock();
    solutions.insert(abs(res));
    det_mutex.unlock();
  }
}

void Calculation::thread_calculate() {

  while (true) {

    unsolved_matrix_mutex.lock();

    if (!threadMatrices.empty()) {

      unique_ptr<ThreadMatrix> a = move(threadMatrices.back());
      threadMatrices.pop_back();
      unsolved_matrix_mutex.unlock();

      pred++;

      array<bool, MAX_N * MAX_N> input;

      for (unsigned i = 0; i < MAX_SIZE; i++)
        input[i] = a->mat[i];

      pi_rep::Matrix m(input, a->size);

      vector<uint8_t> solution;
      m.pi_rep(solution);
      //            m.hex_representation(solution);

      solutions_mutex.lock();

      solutions_rep.insert(solution);
      solutions_mutex.unlock();
      //       int res = det(a->mat, a->size);

      //            if (res != 0) {
      //              solutions_mutex.lock();
      //              solutions.insert(abs(res));
      //              solutions_mutex.unlock();
      //            }
    } else {
      unsolved_matrix_mutex.unlock();
      if (program_end)
        break;
    }
  }
}

class Calculation::Matrix {
public:
  char mat[MAX_SIZE] = {0};
  size_t size;
  vector<uint8_t> numbers;
  vector<int> border;
  set<uint8_t> solutions;

  explicit Matrix(int w) : size(w) {}

  void clearMatrix() {
    numbers.clear();
    border.clear();
  }

  void assignNumber(int num, int curr_lvl) {
    --num;
    numbers.push_back(num);

    for (size_t i = 0; i < size; i++)
      mat[curr_lvl * size + i] = ((num & (1 << (size - 1 - i))) != 0);
    if (curr_lvl == 0)
      for (size_t i = 0; i < size; i++)
        if (mat[i] == 1) {
          border.push_back(i);
          break;
        }
  }

  ~Matrix() = default;

  int number_of_ones(const uint8_t &number) {
    int number_of_ones = 0;
    for (size_t i = 0; i < size; i++)
      number_of_ones += (1 & ((number) >> i));

    return number_of_ones;
  }

  bool checkNumbers(bool last_lvl) {

    auto it2 = *(numbers.end() - 1);

    int number_of_ones_end = number_of_ones(it2);
    int number_of_ones_begin = number_of_ones(*numbers.begin());

    for (auto it = numbers.begin(); it != numbers.end() - 1; it++) {

      // Special case for second level
      // As first level has a point where it switches from 0 to 1
      // second level must be in form of [0,0,...1,1,...,1]
      // from both intervals [0,l], [l+1,n] where l is the index where
      // the border is
      if (numbers.size() == 2) {
        bool start_1 = (mat[1 * size] == 0);
        bool start_2 = true;
        for (int i = 0; i < border.at(0); i++) {
          if (mat[1 * size + i] == 0 && start_1) {
            start_1 = false;
          } else if (!start_1 && start_2) {
            if (mat[1 * size + i] == 1)
              start_2 = false;
          } else if (mat[1 * size + i] == 0) {
            numbers.pop_back();
            return false;
          }
        }

        start_1 = (mat[1 * size + border.at(0)] == 0);
        start_2 = true;

        for (size_t i = border.at(0); i < size; i++) {

          if (mat[1 * size + i] == 0 && start_1) {
            start_1 = false;
          } else if (!start_1 && start_2) {
            if (mat[1 * size + i] == 1)
              start_2 = false;
          } else if (mat[1 * size + i] == 0) {
            numbers.pop_back();
            return false;
          }
        }
      }

      int mask = (*it) & (it2);

      // If any of the rows can be substracted from the new row
      // We don't check that matrix
      if (mask == *it || mask == it2) {
        numbers.pop_back();
        return false;
      }

      // If any of the rows has smaller number of 1's
      // We don't check that matrix
      if (number_of_ones(*it) > number_of_ones_end) {
        numbers.pop_back();
        return false;
      }
    }

    int s = numbers.size();

    pi_rep::Matrix m(numbers, s, size);

    vector<uint8_t> solution;
    m.pi_rep(solution);

    if (solution < numbers) {
      numbers.pop_back();
      return false;
    }

    // If we are on the last level, we do some more checks
    if (last_lvl) {
      vector<int> sums;
      vector<int> trans_numbers;
      // Check if number of ones is smaller than number of ones on the
      //          first row
      for (size_t i = 0; i < size; i++) {
        int sum = 0;
        int num = 0;
        for (size_t j = 0; j < size; j++) {
          sum += mat[j * size + i];
          num = (num << 1) + mat[j * size + i];
        }
        sums.push_back(sum);
        trans_numbers.push_back(num);
        if (sum < number_of_ones_begin) {
          // cout<<"REZ "<<rez<<endl;
          numbers.pop_back();
          return false;
        }
      }
      // Check if any of the numbers has less ones than any in the trans
      //          matrix
      sort(sums.begin(), sums.end());
      sort(trans_numbers.begin(), trans_numbers.end());
      for (size_t i = 0; i < numbers.size() - 1; i++)
        if (trans_numbers[i] == trans_numbers[i - 1]) {
          numbers.pop_back();
          return false;
        }
      bool small_found = false;
      for (size_t i = 0; i < numbers.size(); i++) {
        if (number_of_ones(numbers[i]) > sums[i] ||
            (numbers[i] > trans_numbers[i] && !small_found)) {

          numbers.pop_back();
          return false;
        }
        if (numbers[i] < trans_numbers[i])
          small_found = true;
      }
    }

    return true;
  }

  void find_canon(size_t curr_lvl) {

    int max = pow(2, size) - 1;
    // start from last_level_value + 1
    for (int i = *(numbers.end() - 1) + 1; i < max; i++) {
      // Assign number to the row
      assignNumber(i + 1, curr_lvl);
      bool last_lvl = (curr_lvl == (size - 1));

      // Check if any condition is false
      if (!checkNumbers(last_lvl))
        continue;

      // If we are on last level and all checks are good,
      // we add the matrix to the set
      if (last_lvl) {

        while (threadMatrices.size() * matrixSize >= STACK_MAX) {
          usleep(200);
        }

        unsolved_matrix_mutex.lock();
        threadMatrices.push_back(
            unique_ptr<ThreadMatrix>(new ThreadMatrix(mat, size)));
        unsolved_matrix_mutex.unlock();

      } else
        find_canon(curr_lvl + 1);

      numbers.pop_back();
    }
  }
};

int Calculation::calculation(unsigned test_case) {

  this->matrixSize = sizeof(int) * test_case * test_case;
  program_end = false;

  solutions.insert(0);
  solutions.insert(1);

  vector<thread> threads;

  for (unsigned i = 0; i < THREAD_MAX; i++)
    threads.push_back(thread(&Calculation::thread_calculate, this));

  // Create new matrix with size test_case x test_case
  auto m = new Matrix(test_case);

  int border = test_case % 2 == 0 ? m->size / 2 - 1 : m->size / 2;

  for (int i = 0; i <= border; i++) {
    // Clear current matrix
    m->clearMatrix();
    // Assign (2^i)-1
    m->assignNumber(1 << (i + 1), 0);
    // Start search from next level
    m->find_canon(1);
  }

  program_end = true;
  for (unsigned i = 0; i < THREAD_MAX; i++)
    threads.at(i).join();

  cout << "Matrix count: " << solutions_rep.size() << endl;

  threads.clear();

  for (unsigned i = 0; i < THREAD_MAX; i++)
    threads.push_back(thread(&Calculation::thread_det, this));

  for (unsigned i = 0; i < THREAD_MAX; i++)
    threads.at(i).join();

  //  for (auto it : solutions_rep) {
  //    pi_rep::Matrix m(it);
  //    solutions.insert(abs(m.det()));
  //  }

  cout << "Solutions: " << endl;
  for (auto it : solutions)
    cout << int(it) << " ";
  cout << endl;

  return solutions.size() * 2 - 1;
}
