#ifndef MATRIX_H
#define MATRIX_H
#include <array>
#include <bits/stdc++.h>
#include <vector>

#include "config.h"
namespace pi_rep {
class Matrix {
private:
public:
  unsigned size;
  int k = -1;
  std::array<bool, MAX_N * MAX_N> mat;

  Matrix();
  Matrix(std::array<bool, MAX_N * MAX_N> input, int s);
//  Matrix(const Matrix &input, int s);
  Matrix(const std::vector<uint8_t> &input, int s);
  Matrix(const Matrix &input);
  Matrix(const std::vector<uint8_t> &input, bool transpose = false);
  Matrix(const std::vector<uint8_t> &input, int _k, int n);

  const bool &operator()(int row, int column) const {
    return mat[row * size + column];
  }
  bool &operator()(int row, int column) { return mat[row * size + column]; }

  void hex_representation(std::vector<uint8_t> &res, bool transposed = false);
  void next_perm(const Matrix &m, const std::vector<uint8_t> &perm);
  void next_perm2(const Matrix &m, const std::vector<uint8_t> &perm);
  int det();
  int det(std::array<bool, MAX_N * MAX_N> &mat, unsigned n);
  void pi_rep(std::vector<uint8_t> &solution);
  void fi_rep(std::vector<uint8_t> &solution);
  void assign_extend_line(const std::vector<bool> &extend_line);
  void prepare_perm(std::vector<uint8_t> &perm);
  void prepare_perm2(std::vector<uint8_t> &perm);
};

} // namespace pi_rep
#endif // MATRIX_H
