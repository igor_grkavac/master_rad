
clean:
	rm -rf build

release:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Release ../ && $(MAKE) 

debug:
	mkdir -p build
	cd build && cmake -DCMAKE_BUILD_TYPE=Debug ../ && $(MAKE)
